default: build


build:
	docker build \
		-t sneak/discourse \
		--build-arg UBUNTU_MIRROR="http://ubuntu.datavi.be/ubuntu" \
		. 2>&1 | tee -a build.log
