# focal 2020.01 dev
FROM ubuntu@sha256:d050ed7278c16ff627e4a70d7d353f1a2ec74d8a0b66e5a865356d92f5f6d87b

ENV DEBIAN_FRONTEND noninteractive

ARG UBUNTU_MIRROR=http://archive.ubuntu.com/ubuntu

ARG RUBY_VERSION="2.6.2"
ARG NODE_VERSION="v13.10.1"
# v2.4.0, current stable as of 2020-03-04:
ARG DISCOURSE_VERSION="76b9be3f19f393a216973b791245228f2d3e92f8"

ARG UID=61000
ARG GID=61000

RUN echo "deb $UBUNTU_MIRROR focal main universe restricted multiverse" > /etc/apt/sources.list.new && \
echo "deb $UBUNTU_MIRROR focal-updates main universe restricted multiverse" >> /etc/apt/sources.list.new && \
echo "deb $UBUNTU_MIRROR focal-security main universe restricted multiverse" >> /etc/apt/sources.list.new && \
echo "deb $UBUNTU_MIRROR focal-backports main universe restricted multiverse" >> /etc/apt/sources.list.new && \
mv /etc/apt/sources.list.new /etc/apt/sources.list

# we only install essential system packages here
ARG ADDITIONAL_PKGS=""

RUN \
    apt update && \
    apt -y upgrade && \
    apt -y install \
        $ADDITIONAL_PKGS \
        build-essential \
        ca-certificates \
        daemontools \
        git \
        golang \
        locales \
        rsync \
        runit \
        software-properties-common \
    && \
    go get -v -u git.eeqj.de/sneak/runsvinit && \
    cp /root/go/bin/runsvinit /usr/local/sbin/runsvinit && \
    rm -r /root/go && \
    echo "en_US.UTF-8 UTF-8" > /etc/locale.gen && \
    locale-gen

ENV LANG en_US.UTF-8
ENV LANGUAGE en_US:en
ENV LC_ALL en_US.UTF-8

# prereqs for discourse
RUN apt update && \
    apt -y install \
        advancecomp \
        autoconf \
        automake \
        bison \
        curl \
        debconf-utils \
        expect \
        gawk \
        gifsicle \
        git-core \
        gnupg2 \
        imagemagick \
        jhead \
        jpegoptim \
        libcurl4-openssl-dev \
        libffi-dev \
        libgdbm-dev \
        libjpeg-turbo-progs \
        libncurses5-dev \
        libpcre3 \
        libpcre3-dev \
        libreadline6-dev \
        libsqlite3-dev \
        libssl-dev \
        libtool \
        libyaml-dev \
        openssl \
        optipng \
        pngcrush \
        pngquant \
        python3-software-properties \
        redis-server \
        ruby-full \
        sqlite3 \
        sudo \
        vim \
        zlib1g-dev

COPY ./install.sh /tmp/install.sh
RUN /bin/bash /tmp/install.sh && rm /tmp/install.sh

COPY ./root /tmp/rootoverlay
RUN rsync -avP /tmp/rootoverlay/ / && \
    rm -rf /tmp/rootoverlay && \
    chmod +x /etc/service/*/run

CMD ["/usr/local/sbin/runsvinit"]
