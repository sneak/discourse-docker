# discourse-docker

This builds single-container Discourse.

# Contributing

Pull requests are welcome.

# Author

sneak &lt;[sneak@sneak.berlin](mailto:sneak@sneak.berlin)&gt;
