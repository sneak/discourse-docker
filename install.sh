#!/bin/bash

export HOME=/root

set -e
set -x


H="/var/lib/discourse"
mkdir -p $H

git clone https://github.com/discourse/discourse.git $H/app
cd $H/app
git checkout $DISCOURSE_VERSION
rm -rf .git

cd /

chown -R $UID:$GID $H

groupadd \
    --system --gid $GID \
    discourse

useradd \
    --system --gid $GID --uid $UID \
    --no-log-init --no-create-home -s /bin/false --home-dir $H \
    discourse

apt-get -yqq update
apt install -y postgresql-common
echo -e "\n" | sh /usr/share/postgresql-common/pgdg/apt.postgresql.org.sh
apt-get -yqq update
apt-get install -y postgresql-client-12 libpq-dev

sudo -H -u discourse bash --login <<EOF

id
env
whoami


curl -sSL https://rvm.io/mpapis.asc | gpg2 --import -
curl -sSL https://rvm.io/pkuczynski.asc | gpg2 --import -
curl -sSL https://get.rvm.io | bash -s stable
echo 'gem: --no-document' >> ~/.gemrc


source ~/.rvm/scripts/rvm
rvm install $RUBY_VERSION
rvm --default use $RUBY_VERSION
gem install bundler mailcatcher rake

curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.35.2/install.sh | bash
source ~/.nvm/nvm.sh
nvm install $NODE_VERSION
nvm alias default $NODE_VERSION
npm install -g svgo

cd ~/app
bundle install
EOF


